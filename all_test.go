// Copyright 2019 The pcre2 Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

//go:generate go run generator.go

package pcre2

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"runtime"
	"strings"
	"testing"

	"github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO) //TODOOK
}

// ============================================================================

// ==== jnml@e5-1650:~/src/modernc.org/pcre2> date ; go version ; go test -v -timeout 24h -bench . -benchmem |& tee log-bench-20190206-1858
// Wed Feb  6 18:58:08 CET 2019
// go version go1.11.4 linux/amd64
// === RUN   Test
// --- PASS: Test (0.00s)
// === RUN   TestRegexReduxCCGo
// --- PASS: TestRegexReduxCCGo (0.03s)
// === RUN   TestRegexReduxGo
// --- PASS: TestRegexReduxGo (0.29s)
// === RUN   TestRegexReduxCGo
// --- PASS: TestRegexReduxCGo (0.27s)
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre2
// BenchmarkRegexReduxCCGo-12    	       1	93951047561 ns/op	   0.54 MB/s	1186104464 B/op	   21318 allocs/op
// BenchmarkRegexReduxGo-12      	       1	23559798135 ns/op	   2.16 MB/s	1186046760 B/op	   21839 allocs/op
// BenchmarkRegexReduxCGo-12     	       1	12191455528 ns/op	   4.17 MB/s	1188782520 B/op	  105018 allocs/op
// PASS
// ok  	modernc.org/pcre2	130.321s

// ==== jnml@4670:~/src/modernc.org/pcre2> date ; go version ; go test -timeout 24h -run @ -bench . |& tee log-bench-20190209-1040
// So úno  9 10:40:00 CET 2019
// go version devel +4b3f04c63b Thu Jan 10 18:15:48 2019 +0000 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre2
// BenchmarkRegexReduxCCGo-4   	       1	101007152442 ns/op	   0.50 MB/s
// BenchmarkRegexReduxGo-4     	       1	20328704828 ns/op	   2.50 MB/s
// BenchmarkRegexReduxCGo-4    	       1	14785667715 ns/op	   3.44 MB/s
// PASS
// ok  	modernc.org/pcre2	136.157s

// ==== jnml@4670:~/src/modernc.org/pcre2> date ; go version ; go test -timeout 24h -run @ -bench . |& tee log-bench-20190209-2216
// So úno  9 22:16:51 CET 2019
// go version devel +4b3f04c63b Thu Jan 10 18:15:48 2019 +0000 linux/amd64
// goos: linux
// goarch: amd64
// pkg: modernc.org/pcre2
// BenchmarkRegexReduxCCGo-4   	       1	102344094553 ns/op	   0.50 MB/s
// BenchmarkRegexReduxGo-4     	       1	19401976704 ns/op	   2.62 MB/s
// BenchmarkRegexReduxCGo-4    	       1	14363030633 ns/op	   3.54 MB/s
// PASS
// ok  	modernc.org/pcre2	136.143s

var (
	testData      []byte
	shortTestData []byte
)

func init() {
	buf, err := ioutil.ReadFile("regexredux-input5000000.txt")
	if err != nil {
		panic(err)
	}

	testData = buf
	shortTestData = buf[:1e6]
}

func Test(t *testing.T) {
	for iTest, v := range []struct {
		re, subj string
		m        []uint64
	}{
		{"(a+)[^a]+(a+)[^a]+(a+)", "AaBaaCaaaD", []uint64{1, 9, 1, 2, 3, 5, 6, 9}},
		//                          0123456789
	} {
		func() {
			re, err := Compile(v.re, 0)
			if err != nil {
				t.Fatal(iTest, err)
			}

			defer re.free()

			var m [30]uint64
			n, err := re.Exec([]byte(v.subj), 0, m[:])
			if err != nil {
				t.Fatal(iTest, err)
			}

			if g, e := 2*n, len(v.m); g != e {
				t.Fatalf("%v %v %v", iTest, m[:g], v.m)
			}

			for i, g := range m[:2*n] {
				if e := v.m[i]; g != e {
					t.Fatal(iTest, m[:2*n], v.m)
				}
			}
		}()
	}
}

func TestRegexReduxCCGo(t *testing.T) {
	buf := shortTestData
	var ilen, clen, result int
	var out []int
	var err error
	ilen = len(buf)
	// Delete the comment lines and newlines
	if buf, err = MustCompile("(>[^\n]+)?\n", 0).ReplaceAll(buf, nil, 0); err != nil {
		t.Fatal(err)
	}
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	for i, s := range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(ss string) {
			n, err := countMatchesCCGo(ss, buf)
			if err != nil {
				t.Error(err)
			}

			ch <- n
		}(s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb, err = MustCompile(sub.pat, 0).ReplaceAll(bb, []byte(sub.repl), 0)
			if err != nil {
				t.Error(err)
			}
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i := range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range []int{0, 0, 0, 0, 0, 0, 0, 0, 0} {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, 1000000; g != e {
		t.Error(g, e)
	}

	if g, e := clen, 983585; g != e {
		t.Error(g, e)
	}

	if g, e := result, 983585; g != e {
		t.Error(g, e)
	}
}

func TestRegexReduxGo(t *testing.T) {
	buf := shortTestData
	var ilen, clen, result int
	var out []int
	ilen = len(buf)
	// Delete the comment lines and newlines
	buf = regexp.MustCompile("(>[^\n]+)?\n").ReplaceAll(buf, []byte{})
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	for i, s := range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(ss string) {
			ch <- countMatchesGo(ss, buf)
		}(s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb = regexp.MustCompile(sub.pat).ReplaceAll(bb, []byte(sub.repl))
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i := range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range []int{0, 0, 0, 0, 0, 0, 0, 0, 0} {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, 1000000; g != e {
		t.Error(g, e)
	}

	if g, e := clen, 983585; g != e {
		t.Error(g, e)
	}

	if g, e := result, 983585; g != e {
		t.Error(g, e)
	}
}

func TestRegexReduxCGo(t *testing.T) {
	buf := shortTestData
	var ilen, clen, result int
	var out []int
	ilen = len(buf)
	// Delete the comment lines and newlines
	buf = pcre.MustCompile("(>[^\n]+)?\n", 0).ReplaceAll(buf, []byte{}, 0)
	clen = len(buf)

	mresults := make([]chan int, len(variants))
	var i int
	var s string
	for i, s = range variants {
		ch := make(chan int)
		mresults[i] = ch
		go func(intch chan int, ss string) {
			intch <- countMatchesCGo(ss, buf)
		}(ch, s)
	}

	lenresult := make(chan int)
	bb := buf
	go func() {
		for _, sub := range substs {
			bb = pcre.MustCompile(sub.pat, 0).ReplaceAll(bb, []byte(sub.repl), 0)
		}
		lenresult <- len(bb)
	}()

	out = out[:0]
	for i = range variants {
		out = append(out, <-mresults[i])
	}
	result = <-lenresult
	for i, e := range []int{0, 0, 0, 0, 0, 0, 0, 0, 0} {
		if g := out[i]; g != e {
			t.Error(i, g, e)
		}
	}
	if g, e := ilen, 1000000; g != e {
		t.Error(g, e)
	}

	if g, e := clen, 983585; g != e {
		t.Error(g, e)
	}

	if g, e := result, 983585; g != e {
		t.Error(g, e)
	}
}
