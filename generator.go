// Copyright 2019 The pcre2 Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

// +build none

package main

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

func main() {
	log.SetFlags(log.Flags() | log.Lshortfile)
	if err := main1(); err != nil {
		log.Fatal(err)
	}
}

func main1() error {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		return err
	}

	defer os.RemoveAll(dir)

	if err := untar(dir, "pcre2-10.32.tar.gz"); err != nil {
		return err
	}

	a, o, err := make(filepath.Join(dir, "pcre2-10.32"))
	if err != nil {
		return err
	}

	m := map[string]string{}
	for _, v := range a {
		pkgName := filepath.Base(v)
		pkgName = pkgName[len("lib") : len(pkgName)-len(".a")]
		m[strings.Replace(pkgName, "2-", "", -1)] = v
	}

	if err := makePackage("internal/pcre", m["pcre8"], m["pcreposix"]); err != nil {
		return err
	}

	if err := makePackage("internal/pcre16", m["pcre16"]); err != nil {
		return err
	}

	if err := makePackage("internal/pcre32", m["pcre32"]); err != nil {
		return err
	}

	n := map[string]string{}
	for _, v := range o {
		n[filepath.Base(v)] = v
	}

	if err := makePackage("internal/pcretest", n["pcre2test-pcre2test.o"], m["pcre8"], m["pcre16"], m["pcre32"]); err != nil {
		return err
	}

	return nil
}

func makePackage(pkgDir string, obj ...string) error {
	pkgName := filepath.Base(pkgDir)
	if err := os.MkdirAll(pkgDir, 0770); err != nil {
		return err
	}

	args := []string{"--ccgo-pkg-name", pkgName, "-o", filepath.Join(pkgDir, fmt.Sprintf("%s_%s_%s.go", pkgName, runtime.GOOS, runtime.GOARCH))}
	cmd := exec.Command("ccgo", append(args, obj...)...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func make(dir string) (a, o []string, err error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, nil, err
	}

	defer func() {
		if e := os.Chdir(wd); e != nil && err == nil {
			err = e
		}
	}()

	if err = os.Chdir(dir); err != nil {
		return nil, nil, err
	}

	j := os.Getenv("MAKEJ")
	cmd := exec.Command(
		"/usr/bin/sh", "-c",
		fmt.Sprintf("./configure CC=ccgo CFLAGS='--ccgo-define-values' --disable-shared --disable-cpp --enable-utf --enable-unicode-properties --enable-pcre2-16 --enable-pcre2-32 --disable-pcre2grep-callout && make %s check", j),
	)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return nil, nil, err
	}

	if a, err = filepath.Glob(filepath.Join(dir, ".libs", "*.a")); err != nil {
		return nil, nil, err
	}

	if o, err = filepath.Glob(filepath.Join(dir, "src", "*.o")); err != nil {
		return nil, nil, err
	}

	return a, o, nil
}

func untar(dir, tnm string) error {
	gf, err := os.Open(tnm)
	if err != nil {
		return fmt.Errorf("%s\nPlease download the PCRE archive from https://ftp.pcre.org/pub/pcre/%s", err, tnm)
	}

	gr, err := gzip.NewReader(bufio.NewReader(gf))
	if err != nil {
		return err
	}

	tr := tar.NewReader(gr)
	for {
		hdr, err := tr.Next()
		if err != nil {
			if err != io.EOF {
				return err
			}

			return nil
		}

		switch hdr.Typeflag {
		case tar.TypeDir:
			if err = os.MkdirAll(filepath.Join(dir, hdr.Name), 0770); err != nil {
				return err
			}
		case tar.TypeReg, tar.TypeRegA:
			f, err := os.OpenFile(filepath.Join(dir, hdr.Name), os.O_CREATE|os.O_WRONLY, os.FileMode(hdr.Mode))
			if err != nil {
				return err
			}

			w := bufio.NewWriter(f)
			if _, err = io.Copy(w, tr); err != nil {
				return err
			}

			if err := w.Flush(); err != nil {
				return err
			}

			if err := f.Close(); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unexpected tar header typeflag %#02x", hdr.Typeflag)
		}
	}
}
